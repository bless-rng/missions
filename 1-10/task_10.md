# Задание 10
### Даны натуральные числа от X до Y(включительно). Вывести на консоль те из них, которые при делении на Z дают остаток 1, 2 или 5. Сделать проверку перед выполнением (после ввода) на возможный не правильный ввод чисел X, Y, Z.

Ввод:
    X=1
    Y=21
    Z=4

Вывод
    Натуральные числа от X=1 до Y=21 при делении на Z=4, которые дают остато 1, 2 или 5: [1, 2, 5, 6, 9, 10, 13, 14, 17, 18, 21]
