# Задание 7
### Запрограммировать следующее выражение: (а + b — f / а) + f * a^3 — (a + b) Числа а, b, f вводятся с клавиатуры. Организовать пользовательский интерфейс таким образом, чтобы было понятно, в каком порядке должны вводиться числа.

Пример:
    Введите цифры a, b и f для формулы (a + b - f / a) + f * a^3 - (a + b) через пробел: //здесь должен быть ввод с клавиатуры
    a=15, b=10, f=3
    Вычисляем по формуле: x=(a + b - f / a) + f * a * a - (a + b)
    x=675
